set nocompatible 
set showcmd
set showmatch
set ruler
set showmode
set notitle
set wildmode=longest:full
set wildmenu
set hidden
set modeline

syntax on

set expandtab
set shiftwidth=4
set tabstop=4
